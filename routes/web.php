<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

\Illuminate\Support\Facades\Auth::routes(["register" => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware("auth")->group(function () {
    Route::resource("/users", "UserController");
    
    Route::get("/photoTypes", "PhotoTypeController@index")->name('photoTypes.index');
    Route::get("/photoTypes/search/{type}", "PhotoTypeController@index");
    Route::get("/photoTypes/create", "PhotoTypeController@create")->name('photoTypes.create');
    Route::post("/photoTypes", "PhotoTypeController@store")->name('photoTypes.store');
    Route::resource("/photoTypes", "PhotoTypeController")->except(["index"]);

    Route::resource("/photos", "PhotoController");
    Route::post("/photos/uploadFile", "PhotoController@uploadFile");
    Route::post("/photos/deleteFile", "PhotoController@deleteFile");
    Route::post("/photos/deletePicture/{route}", "PhotoController@deletePicture");

    Route::resource("/logos", "LogoController");
    Route::post("/logos/uploadFile", "LogoController@uploadFile");
    Route::post("/logos/deleteFile", "LogoController@deleteFile");
       
    Route::post("/novedades/uploadFile", "NovedadController@uploadFile");
    Route::post("/novedades/deleteFile", "NovedadController@deleteFile");
    Route::resource("/novedades", "NovedadController")->parameters(['novedades' => 'novedad']);

    Route::resource("/vacantes","VacanteController");
        
    Route::resource("/settings", "SettingController")->except("show");
});
