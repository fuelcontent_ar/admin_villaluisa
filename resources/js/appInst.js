/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/*window.Vue = require('vue');*/

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/Editor.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('editor-js', require('./components/Editor.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import datepickerFactory from 'jquery-datepicker';
import datepickerEsFactory from 'jquery-datepicker/i18n/jquery.ui.datepicker-es';

datepickerFactory($);
datepickerEsFactory($);

import EditorJS from '@editorjs/editorjs';
import ImageTool from '@editorjs/image';
import Header from '@editorjs/header';
import Delimiter from '@editorjs/delimiter';
import List from '@editorjs/list';
import LinkTool from '@editorjs/link';
import Table from '@editorjs/table';
const Paragraph = require('@editorjs/paragraph');


$(document).ready(function() {
    window.editor = new EditorJS({
        autofocus: true,
        tools: {
            header: {
                class: Header,
                config: {
                    placeholder: 'Ingrese un título',
                    levels: [2],
                    defaultLevel: 2
                }
            },
            
            list: {
                class: List,
                inlineToolbar: true,
            },
            
            
        },
        i18n: {
            messages: {
                /**
                 * Other below: translation of different UI components of the editor.js core
                 */
                ui: {
                    "blockTunes": {
                        "toggler": {
                        "Click to tune": "Click para ajustar",
                        "or drag to move": "o arrastrar para mover"
                        },
                    },
                    "inlineToolbar": {
                        "converter": {
                            "Convert to": "Convertir a"
                        }
                    },
                    "toolbar": {
                        "toolbox": {
                            "Add": "Agregar"
                        }
                    }
                },
                toolNames: {
                    "Text": "Párrafo",
                    "Heading": "Titular",
                    "List": "Lista",
                    "Delimiter": "Separador",
                    "Link": "Vínculo",
                    "Bold": "Negrita",
                    "Italic": "Itálica",
                    
                },
                blockTunes: {
                    "delete": {
                      "Delete": "Borrar"
                    },
                    "moveUp": {
                      "Move up": "Mover Arriba"
                    },
                    "moveDown": {
                      "Move down": "Mover Abajo"
                    }
                  },
            },
        },
        data: JSON.parse($('#articleBody').val()),
    })
})