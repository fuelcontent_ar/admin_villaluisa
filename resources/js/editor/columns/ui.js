//import buttonIcon from './svg/button-icon.svg';

/**
 * Class for working with UI:
 *  - rendering base structure
 *  - show/hide preview
 *  - apply tune view
 */
export default class Ui {
  /**
   * @param {object} ui - image tool Ui module
   * @param {object} ui.api - Editor.js API
   * @param {ImageConfig} ui.config - user config
   * @param {Function} ui.onSelectFile - callback for clicks on Select file button
   * @param {boolean} ui.readOnly - read-only mode flag
   */
  constructor({ api, config, onSelectFileLeft, onSelectFileRight, readOnly }) {
    this.api = api;
    this.config = config;
    this.onSelectFileLeft = onSelectFileLeft;
    this.onSelectFileRight = onSelectFileRight;
    this.readOnly = readOnly;
    this.nodes = {
      wrapper: make('div', [this.CSS.baseClass, this.CSS.wrapper, this.CSS.wrapperTwoCols]),
      imageContainerLeft: make('div', [ this.CSS.imageContainer, this.CSS.imageContainerLeft ]),
      imageContainerRight: make('div', [ this.CSS.imageContainer, this.CSS.imageContainerRight ]),
      fileButtonLeft: this.createFileButtonLeft(),
      fileButtonRight: this.createFileButtonRight(),
      imageElRight: undefined,
      imageElLeft: undefined,
      imagePreloaderLeft: make('div', this.CSS.imagePreloader),
      imagePreloaderRight: make('div', this.CSS.imagePreloader),
      captionLeft: make('div', [this.CSS.input, this.CSS.caption], {
        contentEditable: !this.readOnly,
      }),
      captionRight: make('div', [this.CSS.input, this.CSS.caption], {
        contentEditable: !this.readOnly,
      }),
      columnLeft: make('div', [this.CSS.column, this.CSS.columnleft]),
      columnRight: make('div', [this.CSS.column, this.CSS.columnright])
    };

    /**
     * Create base structure
     *  <wrapper>
     *    <column>
     *     <image-container>
     *       <image-preloader />
     *     </image-container>
     *     <caption />
     *     <select-file-button />
     *    </column>
     *    <column>
     *     <image-container>
     *       <image-preloader />
     *     </image-container>
     *     <caption />
     *     <select-file-button />
     *    </column>
     *  </wrapper>
     */

    this.nodes.captionLeft.dataset.placeholder = this.config.captionPlaceholder;
    this.nodes.captionRight.dataset.placeholder = this.config.captionPlaceholder;

    this.nodes.imageContainerLeft.appendChild(this.nodes.imagePreloaderLeft);
    this.nodes.imageContainerRight.appendChild(this.nodes.imagePreloaderRight);

    this.nodes.columnLeft.appendChild(this.nodes.imageContainerLeft);
    this.nodes.columnRight.appendChild(this.nodes.imageContainerRight);

    this.nodes.columnLeft.appendChild(this.nodes.captionLeft);
    this.nodes.columnRight.appendChild(this.nodes.captionRight);

    this.nodes.columnLeft.appendChild(this.nodes.fileButtonLeft);
    this.nodes.columnRight.appendChild(this.nodes.fileButtonRight);

    this.nodes.wrapper.appendChild(this.nodes.columnLeft);
    this.nodes.wrapper.appendChild(this.nodes.columnRight);
  }

  /**
   * CSS classes
   *
   * @returns {object}
   */
  get CSS() {
    return {
      baseClass: this.api.styles.block,
      loading: this.api.styles.loader,
      input: this.api.styles.input,
      button: this.api.styles.button,

      /**
       * Tool's classes
       */
      wrapper: 'image-tool',
      wrapperTwoCols: 'image-tool--twocols',
      imageContainer: 'image-tool__image',
      imageContainerLeft: 'image-tool__imageleft',
      imageContainerRight: 'image-tool__imageright',
      imagePreloader: 'image-tool__image-preloader',
      imageEl: 'image-tool__image-picture',
      captionLeft: 'image-tool__caption',
      captionRight: 'image-tool__caption',
      column: 'image-tool__column',
      columnleft: 'image-tool__columnleft',
      columnright: 'image-tool__columnright',
    };
  };

  /**
   * Ui statuses:
   * - empty
   * - uploading
   * - filled
   *
   * @returns {{EMPTY: string, UPLOADING: string, FILLED: string}}
   */
  static get status() {
    return {
      EMPTY: 'empty',
      EMPTYRIGHT: 'emptyright',
      EMPTYLEFT: 'emptyleft',
      UPLOADING: 'loading',
      UPLOADINGRIGHT: "uploadingright",
      UPLOADINGLEFT: "uploadingleft",
      FILLED: 'filled',
      FILLEDRIGHT: 'filledright',
      FILLEDLEFT: 'filledleft',
    };
  }

  /**
   * Renders tool UI
   *
   * @param {ImageToolData} toolData - saved tool data
   * @returns {Element}
   */
  render(toolData) {
    console.log(toolData);
    if (!toolData.fileLeft || Object.keys(toolData.fileLeft).length === 0) {
      this.toggleStatus(Ui.status.EMPTYLEFT);
    } else {
      this.toggleStatus(Ui.status.UPLOADINGLEFT);
    }
    if (!toolData.fileRight || Object.keys(toolData.fileRight).length === 0){
      this.toggleStatus(Ui.status.EMPTYRIGHT);
    }else{
      this.toggleStatus(Ui.status.UPLOADINGRIGHT);
    }

    return this.nodes.wrapper;
  }

  /**
   * Creates upload-file button
   *
   * @returns {Element}
   */
  createFileButtonLeft() {
    const button = make('div', [ this.CSS.button ]);
    const buttonIcon = '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M3.15 13.628A7.749 7.749 0 0 0 10 17.75a7.74 7.74 0 0 0 6.305-3.242l-2.387-2.127-2.765 2.244-4.389-4.496-3.614 3.5zm-.787-2.303l4.446-4.371 4.52 4.63 2.534-2.057 3.533 2.797c.23-.734.354-1.514.354-2.324a7.75 7.75 0 1 0-15.387 1.325zM10 20C4.477 20 0 15.523 0 10S4.477 0 10 0s10 4.477 10 10-4.477 10-10 10z"/></svg>';

    button.innerHTML = this.config.buttonContent || `${buttonIcon} ${this.api.i18n.t('Select an Image')}`;

    button.addEventListener('click', () => {
      this.onSelectFileLeft();
    });

    return button;
  }
  createFileButtonRight() {
    const button = make('div', [ this.CSS.button ]);
    const buttonIcon = '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M3.15 13.628A7.749 7.749 0 0 0 10 17.75a7.74 7.74 0 0 0 6.305-3.242l-2.387-2.127-2.765 2.244-4.389-4.496-3.614 3.5zm-.787-2.303l4.446-4.371 4.52 4.63 2.534-2.057 3.533 2.797c.23-.734.354-1.514.354-2.324a7.75 7.75 0 1 0-15.387 1.325zM10 20C4.477 20 0 15.523 0 10S4.477 0 10 0s10 4.477 10 10-4.477 10-10 10z"/></svg>';

    button.innerHTML = this.config.buttonContent || `${buttonIcon} ${this.api.i18n.t('Select an Image')}`;

    button.addEventListener('click', () => {
      this.onSelectFileRight();
    });

    return button;
  }

  /**
   * Shows uploading preloader
   *
   * @param {string} src - preview source
   * @returns {void}
   */
  showPreloaderLeft(src) {
    this.nodes.imagePreloaderLeft.style.backgroundImage = `url(${src})`;

    this.toggleStatus(Ui.status.UPLOADINGLEFT);
  }

  showPreloaderRight(src) {
    this.nodes.imagePreloaderRight.style.backgroundImage = `url(${src})`;

    this.toggleStatus(Ui.status.UPLOADINGRIGHT);
  }

  /**
   * Hide uploading preloader
   *
   * @returns {void}
   */
  hidePreloaderLeft() {
    this.nodes.imagePreloaderLeft.style.backgroundImage = '';
    this.toggleStatus(Ui.status.EMPTYLEFT);
  }

  /**
   * Hide uploading preloader
   *
   * @returns {void}
   */
  hidePreloaderRight() {
    this.nodes.imagePreloaderRight.style.backgroundImage = '';
    this.toggleStatus(Ui.status.EMPTYRIGHT);
  }

  /**
   * Shows an image
   *
   * @param {string} url - image source
   * @returns {void}
   */
  fillImageLeft(url) {
    /**
     * Check for a source extension to compose element correctly: video tag for mp4, img — for others
     */
    const tag = /\.mp4$/.test(url) ? 'VIDEO' : 'IMG';

    const attributes = {
      src: url,
    };

    /**
     * We use eventName variable because IMG and VIDEO tags have different event to be called on source load
     * - IMG: load
     * - VIDEO: loadeddata
     *
     * @type {string}
     */
    let eventName = 'load';

    /**
     * Update attributes and eventName if source is a mp4 video
     */
    if (tag === 'VIDEO') {
      /**
       * Add attributes for playing muted mp4 as a gif
       *
       * @type {boolean}
       */
      attributes.autoplay = true;
      attributes.loop = true;
      attributes.muted = true;
      attributes.playsinline = true;

      /**
       * Change event to be listened
       *
       * @type {string}
       */
      eventName = 'loadeddata';
    }

    /**
     * Compose tag with defined attributes
     *
     * @type {Element}
     */
    this.nodes.imageElLeft = make(tag, this.CSS.imageEl, attributes);

    /**
     * Add load event listener
     */
    this.nodes.imageElLeft.addEventListener(eventName, () => {
      this.toggleStatus(Ui.status.FILLEDLEFT);

      /**
       * Preloader does not exists on first rendering with presaved data
       */
      if (this.nodes.imagePreloaderLeft) {
        this.nodes.imagePreloaderLeft.style.backgroundImage = '';
      }
    });

    this.nodes.imageContainerLeft.appendChild(this.nodes.imageElLeft);
  }

  /**
   * Shows an image
   *
   * @param {string} url - image source
   * @returns {void}
   */
  fillImageRight(url) {
    /**
     * Check for a source extension to compose element correctly: video tag for mp4, img — for others
     */
    const tag = /\.mp4$/.test(url) ? 'VIDEO' : 'IMG';

    const attributes = {
      src: url,
    };

    /**
     * We use eventName variable because IMG and VIDEO tags have different event to be called on source load
     * - IMG: load
     * - VIDEO: loadeddata
     *
     * @type {string}
     */
    let eventName = 'load';

    /**
     * Update attributes and eventName if source is a mp4 video
     */
    if (tag === 'VIDEO') {
      /**
       * Add attributes for playing muted mp4 as a gif
       *
       * @type {boolean}
       */
      attributes.autoplay = true;
      attributes.loop = true;
      attributes.muted = true;
      attributes.playsinline = true;

      /**
       * Change event to be listened
       *
       * @type {string}
       */
      eventName = 'loadeddata';
    }

    /**
     * Compose tag with defined attributes
     *
     * @type {Element}
     */
    this.nodes.imageElRight = make(tag, this.CSS.imageEl, attributes);

    /**
     * Add load event listener
     */
    this.nodes.imageElRight.addEventListener(eventName, () => {
      this.toggleStatus(Ui.status.FILLEDRIGHT);

      /**
       * Preloader does not exists on first rendering with presaved data
       */
      if (this.nodes.imagePreloaderRight) {
        this.nodes.imagePreloaderRight.style.backgroundImage = '';
      }
    });

    this.nodes.imageContainerRight.appendChild(this.nodes.imageElRight);
  }

  /**
   * Shows caption input
   *
   * @param {string} text - caption text
   * @returns {void}
   */
  fillCaptionLeft(text) {
    if (this.nodes.captionLeft) {
      this.nodes.captionLeft.innerHTML = text;
    }
  }
  fillCaptionRight(text) {
    if (this.nodes.captionRight) {
      this.nodes.captionRight.innerHTML = text;
    }
  }

  /**
   * Changes UI status
   *
   * @param {string} status - see {@link Ui.status} constants
   * @returns {void}
   */
  toggleStatus(status) {
    //aca hacemos chanchadas para setear el filled a mano
    switch(status){
      case Ui.status.FILLEDLEFT:
        this.clearAllStatusLeft();
        this.nodes.columnLeft.classList.toggle(`${this.CSS.columnleft}--filled`);
      break;
      case Ui.status.FILLEDRIGHT:
        this.clearAllStatusRight();
        this.nodes.columnRight.classList.toggle(`${this.CSS.columnright}--filled`);
      break;
      case Ui.status.UPLOADINGLEFT:
        this.clearAllStatusLeft();
        this.nodes.columnLeft.classList.toggle(`${this.CSS.columnleft}--loading`);
      break;
      case Ui.status.UPLOADINGRIGHT:
        this.clearAllStatusRight();
        this.nodes.columnRight.classList.toggle(`${this.CSS.columnright}--loading`);
      break;
      case Ui.status.EMPTYLEFT:
        this.clearAllStatusLeft();
        this.nodes.columnLeft.classList.toggle(`${this.CSS.columnleft}--empty`);
      break;
      case Ui.status.EMPTYRIGHT:
        this.clearAllStatusRight();
        this.nodes.columnRight.classList.toggle(`${this.CSS.columnright}--empty`);
      break;
      default: //this will handle normal status behaviour
        for (const statusType in Ui.status) {
          if (Object.prototype.hasOwnProperty.call(Ui.status, statusType)) {
            this.nodes.wrapper.classList.toggle(`${this.CSS.wrapper}--${Ui.status[statusType]}`, status === Ui.status[statusType]);
          }
        }
      break;
    }
    /**/
  }

  clearAllStatusLeft(){
    for (const statusType in Ui.status) {
      this.nodes.columnLeft.classList.remove(`${this.CSS.columnleft}--${Ui.status[statusType]}`);
    }
  }
  clearAllStatusRight(){
    for (const statusType in Ui.status) {
      this.nodes.columnRight.classList.remove(`${this.CSS.columnright}--${Ui.status[statusType]}`);
    }
  }

  /**
   * Apply visual representation of activated tune
   *
   * @param {string} tuneName - one of available tunes {@link Tunes.tunes}
   * @param {boolean} status - true for enable, false for disable
   * @returns {void}
   */
  applyTune(tuneName, status) {
    this.nodes.wrapper.classList.toggle(`${this.CSS.wrapper}--${tuneName}`, status);
  }
}

/**
 * Helper for making Elements with attributes
 *
 * @param  {string} tagName           - new Element tag name
 * @param  {Array|string} classNames  - list or name of CSS class
 * @param  {object} attributes        - any attributes
 * @returns {Element}
 */
export const make = function make(tagName, classNames = null, attributes = {}) {
  const el = document.createElement(tagName);

  if (Array.isArray(classNames)) {
    el.classList.add(...classNames);
  } else if (classNames) {
    el.classList.add(classNames);
  }

  for (const attrName in attributes) {
    el[attrName] = attributes[attrName];
  }

  return el;
};
