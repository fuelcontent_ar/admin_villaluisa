<?php

return [
    "all" => [
        "title" => "Listado del blog",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-show" => "Ver datos del blog",
        "tooltip-edit" => "Editar datos del blog",
        "tooltip-destroy" => "Eliminar blog",
    ],
    "edit" => [
        "title" => "Editar el título del blog",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nuevo título de blog",
        "message" => "Se ha creado el título del blog.",
    ],
    "labels" => [
        "id" => "ID",
        "title" => "Título",
        "category" => "Categorías",
        "articles_count" => "Cantidad de artículos",
        "created_at" => "Creado en",
        "user" => "Autor",
    ],
    "controllers" => [
        "destroy" => "Titulo del blog eliminado con éxito.",
    ],
];
