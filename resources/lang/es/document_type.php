<?php

return [
    "all" => [
        "title" => "Lista de categoría de documentos",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-show" => "Ver categoría del documento",
        "tooltip-edit" => "Editar categoría del documento",
        "tooltip-destroy" => "Eliminar categoría documento",
    ],
    "edit" => [
        "title" => "Editar categoría del documento",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nueva categoría de documento",
        "message" => "Se ha creado el título del documento.",
    ],
    "labels" => [
        "id" => "ID",
        "title" => "Título",
        "created_at" => "Creado en",
        "documents_count" => "Cantidad de documentos",
        "user" => "Autor",
    ],
    "controllers" => [
        "destroy" => "Titulo del documento eliminado con éxito.",
    ],
];
