<?php


return [
    "all" => [
        "title" => "Lista de novedades",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-create" => "Crear novedad",
        "tooltip-show" => "Ver datos de las novedades",
        "tooltip-edit" => "Editar datos de las novedades",
        "tooltip-destroy" => "Eliminar novedades",
    ],
    "edit" => [
        "title" => "Editar novedad",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nueva novedad",
        "message" => "Se ha creado la novedad.",
    ],
    "labels" => [
        "title" => "Título",
        "slug" => "URL",
        "blog" => "Blog",
        "category" => "Categorías",
        "published" => "Publicada",
        "published_at" => "Publicada en",
        "created_at" => "Creada en",
        "description" => "Descripcion",
        "body" => "Contenido",
        "image" => "Imagen principal",
        "highlighted" => "Destacada",
        "hasimageheader" => "Mostrar imagen en encabezado",
        "banner" => "Banner homepage",
        "gallery" => "Galeria de imagenes",
        "input-file" => "Elija el archivo",
        "images" => "Imagenes",
        "lang" => "Lenguaje",
        "video" => "Url video youtube",
        "header_image" => "Imagen header novedad",
        "card_image" => "Imagen interna novedad",

    ],
    "controllers" => [
        "destroy" => "Novedad eliminada con éxito.",
    ],
    "option" => "Opciones de publicacion",
    "enable-disable" => "Publicar",
    "highlight" => "Destacar",
];
