<?php
return [
    "all" => [
        "title" => "Lista de usuarios",
        "disabled" => "El usuario fue deshabilitado exitosamente.",
        "enabled" => "El usuario fue activado correctamente.",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-create" => "Crear usuario",
        "tooltip-show" => "Ver datos de usuario",
        "tooltip-edit" => "Editar datos de usuario",
        "tooltip-destroy" => "Borrar usuario",
        "enable-disable" => "Habilitar o deshabilitar",
        "modal-title" => "Datos del usuario",
    ],
    "edit" => [
        "title" => "Editar usuario",
        "button" => "Editar",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Guardar usuario",
        "button" => "Crear",
        "message" => "El usuario ha sido creado.",
    ],
    "labels" => [
        "id" => "ID",
        "name" => "Nombre",
        "email" => "Correo electrónico",
        "active" => "Activo",
        "last_login" => "Último acceso",
        "password" => "Contraseña",
        "repeat-password" => "Repite la contraseña",
    ],
    "controllers" => [
        "destroy" => "Usuario eliminado con éxito.",
    ],
];
