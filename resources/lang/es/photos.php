<?php


return [
    "all" => [
        "title" => "Lista de fotos",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
        "tooltip-create" => "Crear novedad",
        "tooltip-show" => "Ver datos de las fotos",
        "tooltip-edit" => "Editar datos de las fotos",
        "tooltip-destroy" => "Eliminar fotos",
    ],
    "edit" => [
        "title" => "Editar foto",
        "message" => "Los datos han sido actualizados.",
    ],
    "save" => [
        "title" => "Crear nueva foto",
        "message" => "Se ha creado la foto.",
    ],
    "labels" => [
        "name" => "Nombre",
        "epigraph" => "Epígrafe",
        "file" => "Ruta",
        "order" => "Nro de orden",
        "phototype_id" => "Tipo de foto",
        

    ],
    "controllers" => [
        "destroy" => "Foto eliminada con éxito.",
    ],
    "option" => "Opciones de publicacion",
    "enable-disable" => "Publicar",
    "highlight" => "Destacar",
];
