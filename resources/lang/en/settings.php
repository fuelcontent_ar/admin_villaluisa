<?php

return [
    "all" => [
        "title" => "List of configurations",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/English.json",
        "tooltip-show" => "See configuration data",
        "tooltip-edit" => "Edit configuration data",
        "tooltip-destroy" => "Delete configuration",
    ],
    "edit" => [
        "title" => "Edit settings",
        "button" => "Edit",
        "message" => "The data has been updated.",
    ],
    "save" => [
        "title" => "Create new configuration",
        "button" => "Create",
        "message" => "The blog title has been created.",
    ],
    "labels" => [
        "id" => "ID",
        "key" => "Identifier",
        "user" => "Created by",
        "created_at" => "Created in",
    ],
    "controllers" => [
        "destroy" => "Configuration removed successfully.",
    ],
];
