<?php
return [
    "all" => [
        "title" => "List of Users",
        "disabled" => "User was successfully disabled.",
        "enabled" => "The user was activated correctly.",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/English.json",
        "tooltip-create" => "Create User",
        "tooltip-show" => "View user data",
        "tooltip-edit" => "Edit user data",
        "tooltip-destroy" => "Delete user",
        "enable-disable" => "Enable or disable",
        "modal-title" => "User data",
    ],
    "edit" => [
        "title" => "Edit User",
        "button" => "Edit",
        "message" => "The data has been updated.",
    ],
    "save" => [
        "title" => "Save User",
        "button" => "Create",
        "message" => "The user has been created.",
    ],
    "labels" => [
        "id" => "ID",
        "name" => "Name",
        "email" => "Email",
        "active" => "Active",
        "last_login" => "Last Login",
        "password" => "Password",
        "repeat-password" => "Repeat Password",
    ],
    "controllers" => [
        "destroy" => "User removed successfully.",
    ],
];
