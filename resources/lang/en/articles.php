<?php

return [
    "all" => [
        "title" => "List of Articles",
        "lang-datatables" => "//cdn.datatables.net/plug-ins/1.10.21/i18n/English.json",
        "tooltip-create" => "Create article",
        "tooltip-show" => "See article data",
        "tooltip-edit" => "Edit article data",
        "tooltip-destroy" => "Delete article",
    ],
    "edit" => [
        "title" => "Edit article",
        "message" => "The data has been updated.",
    ],
    "save" => [
        "title" => "Create new article",
        "message" => "The blog title has been created.",
    ],
    "labels" => [
        "title" => "Title",
        "slug" => "URL Slug",
        "blog" => "Blog",
        "category" => "Categories",
        "description" => "Description",
        "published" => "Published",
        "published_at" => "Publish At",
        "created_at" => "Created At",
        "body" => "Content",
        "image" => "Main Image",
        "input-file" => "Choose file",
        "lang" => "Language",
        "video" => "Url video youtube",
        "images" => "Images",
    ],
    "controllers" => [
        "destroy" => "Article removed successfully.",
    ],
    "option" => "Publishing options",
    "enable-disable" => "Publish",
    "highlight" => "Highlight",
];
