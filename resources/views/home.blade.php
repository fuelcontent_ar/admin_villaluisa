@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('home.dashboard') }} - {{ __('home.welcome') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Estos son los recursos disponibles:</p>
                    <div class="d-flex justify-content flex-wrap"></div>
                    
                        <div class="m-2">
                            <a href="{{route("photoTypes.index")}}" class="btn btn-primary">Galería de fotos</a>
                        </div>
                        <div class="m-2">
                            <a href="{{route("logos.index")}}" class="btn btn-primary">Logos de clientes</a>
                        </div>
                        <div class="m-2">
                            <a href="{{route("novedades.index")}}" class="btn btn-primary">Novedades</a>
                        </div>
                        <div class="m-2">
                            <a href="{{route("vacantes.index")}}" class="btn btn-primary">Vacantes</a>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
