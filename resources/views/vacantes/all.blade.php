@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Editor de vacantes
                        <a class="btn btn-success btn-sm float-right" href="{{route('vacantes.create')}}"
                           data-toggle="tooltip" data-placement="right" title="Agregar vacante nueva">
                            <i class="fa fa-plus"></i> Nueva
                        </a>
                    </div>
                    <div class="card-body">
                    <table class="table table-striped table-bordered dataTable no-footer">
                        <thead>
                            <tr>
                                <td><strong>ID</strong></td>
                                <td><strong>Nombre</strong></td>
                                <td><strong>Descripción</strong></td>
                                <td><strong>Acciones</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($vacantes as $vacante)
                            <tr>
                                <td>{{$vacante->id}}</td>
                                <td>{{$vacante->title}}</td>
                                <td>{{$vacante->text}}</td>
                                <td>
                                <a class="btn btn-warning btn-sm" href="http://admin_villaluisa.test/vacantes/{{$vacante->id}}/edit" data-toggle="tooltip" data-placement="right" title="Editar datos de la vacante">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                  <button type="button" class="btn btn-danger btn-sm" onclick="drop({{$vacante->id}})" data-toggle="tooltip" data-placement="right" title="Eliminar vacante">
                                      <i class="fa fa-trash"></i>
                                  </button>
                                </td>
                                
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $vacantes->links() }}    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script>
        var table;
        
        let functions = new Functions();
        $(function () {
            $("#alert").hide();
            $('[data-toggle="tooltip"]').tooltip()
        })

        /** start request delete title blogs **/
        function drop(blogId) {
            
            let response;
            let alert = new Alerts();
            alert.warning(
                "{{__("general.alert-type.warning")}}",
                "{{__("general.delete.message")}}",
                {yes: "{{__("general.buttons.yes")}}", no: "{{__("general.buttons.no")}}"},
                async function () {
                
                response = await functions.request('{{url("vacantes")}}/' + blogId, "DELETE");
                
                if (response) {
                    swal("{{__("general.delete.title")}}", response, "success");
                    setTimeout(function () { 
                        document.location.reload(true); 
                        }, 2000);
                }
            });
        }

        /** end request delete blog **/
        /** start request show blog **/
        function show() {
            clearErrors();
            $("#titleModal").html("{{__("novedades.save.title")}}")
            $("#edit").val("");
            $("#form").trigger("reset");
            $("#modal").modal({keyboard: false})
            
        }
        /** end request show blog **/

        /** start request create title blogs **/
        function create() {
            clearErrors();
            let data = {
                title: $("#title").val(),
                category:  $("#category").val(),
            };
            functions.request('{{route("photos.store")}}', "POST", data, {}, function () {
                $("#text-create").hide();
                $("#text-load").show();
                $("#load").show();
                $("#primary").attr("disabled", "disabled");
            }).then(() => {
                $("#text-create").show();
                $("#text-load").hide();
                $("#primary").removeAttr("disabled");
                $("#load").hide();
                $("#form").trigger("reset");
                table.ajax.reload();
                $("#modal").modal("hide");
                swal("Created!", "{{__("novedades.save.message")}}", "success");
            }).catch(err => {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                for (let error in err.responseJSON.errors) {
                    $("#error_"+error).html(err.responseJSON.errors[error]).show();
                }
            });
        }
        /** end request create blog **/

        /** start request get title blogs **/
        async function get(blogId) {
            let response;
            console.log(blogId);
            clearErrors();
            response = await functions.request('{{url("/photos")}}/'+blogId, "GET");
            
            if (response) {
                $("#titleModal").html("{{__("photos.edit.title")}}")
                $("#edit").val(blogId);
                $("#title").val(response.data.title);
                $("#category").val(response.data.category_id).change();
                $("#modal").modal("show");
            }
        }
        /** end request get blog **/

        /** start request edit title blogs **/
        function edit() {
            clearErrors();
            let blogId = $("#edit").val();
            let data = {
                title: $("#title").val(),
                category:  $("#category").val(),
            };
            functions.request('{{url("/vacantes")}}/'+blogId, "PATCH", data, {}, function () {
                $("#text-create").hide();
                $("#text-load").show();
                $("#load").show();
                $("#primary").attr("disabled", "disabled");
            }).then(() => {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                $("#form").trigger("reset");
                table.ajax.reload();
                $("#modal").modal("hide");
                swal("Updated!", "{{__("novedades.edit.message")}}", "success");
            }).catch(err => {
                $("#text-create").show();
                $("#primary").removeAttr("disabled");
                $("#text-load").hide();
                $("#load").hide();
                for (let error in err.responseJSON.errors) {
                    $("#error_"+error).html(err.responseJSON.errors[error]).show();
                }
            });
        }
        /** end request edit blog **/
        function execute() {
            if ($("#edit").val() === "") {
                return create();
            }
            return edit();
        }

        function clearErrors() {
            $("#error_title").html("").hide();
            $("#error_category").html("").hide();
        }
    </script>
@endsection
