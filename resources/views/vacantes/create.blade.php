@extends('layouts.app')

@section('content')
    <div class="container">
        <form
            action="{{route("vacantes.store")}}"
            method="POST"
            id="form"
            enctype="multipart/form-data"
        >
          
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <span id="message"></span>
                    <div class="card">
                        <div class="card-header">Subir nueva vacante</div>
                        <div class="card-body">
                            @if ($errors->any() || session()->has("message"))
                                @php
                                    $type = session()->has("message") ? "success" : "danger";
                                    $message = session()->has("message") ? session()->get("message") : "Arreglar los siguientes campos:";
                                @endphp    
                                <x-alert :message="$message" :type="$type" :show="true" duration="100000"/>
                            @endif
                            <div class="form-group">
                                <label for="title">Nombre</label>
                                <input type="text"
                                       class="form-control"
                                       id="title"
                                       name="title"
                                       required
                                       placeholder="Nombre"
                                       value=""
                                />
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea 
                                type="text"
                                class="form-control"
                                       id="descripcion"
                                       name="descripcion"
                                       required
                                       placeholder="Descripción"
                                       value=""
                                       rows="5"
                                >
                                </textarea>
                            </div>
                                                
                            <div id="kv-avatar-errors-2"></div>
                            <div class="form-group">
                                <button class="btn btn-primary float-right"
                                    id="save">Guardar nueva vacante</button>
                            </div>
                                          
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
@endsection
@section("js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection
@section("scripts")
    <script type="application/javascript">
        $(document).ready(function() {
            if (!$("#publish").is(':checked')) {
                $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
            }
            $("#publish").change( function(){
                if( $(this).is(':checked') ) {
                    $("#datetime").removeAttr("readOnly").datepicker("option", "disabled", false);
                }else{
                    $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
                }
            });
            var now = moment();
            let date = $("#datetime").val().length < 0 ? $("#datetime").val() : now.format('DD/MM/YYYY');
            $('#datetime').datepicker({
                changeMonth: true,
                changeYear: true,
            });

            $.datepicker.regional['es'];
        });
        
    </script>
    <script type="application/javascript">

        
        let functions = new Functions();
        
                   
            
        });

        function removeItemFromArr(arr, item) {
            let i = arr.indexOf( item );

            if ( i !== -1 ) {
                arr.splice( i, 1 );
            }
        }

        
    </script>
@endsection