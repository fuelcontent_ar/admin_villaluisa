@extends('layouts.app')

@section('content')
    <div class="container">
        <form
            action="{{route("logos.store")}}"
            method="POST"
            id="form"
            enctype="multipart/form-data"
        >
                  
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <span id="message"></span>
                    <div class="card">
                        <div class="card-header">Subir nuevo logo de cliente</div>
                        <div class="card-body">
                            @if ($errors->any() || session()->has("message"))
                                @php
                                    $type = session()->has("message") ? "success" : "danger";
                                    $message = session()->has("message") ? session()->get("message") : "Arreglar los siguientes campos:";
                                @endphp    
                                <x-alert :message="$message" :type="$type" :show="true" duration="100000"/>
                            @endif
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text"
                                       class="form-control"
                                       id="name"
                                       name="name"
                                       required
                                       placeholder="Nombre"
                                       value=""
                                />
                            </div>
                            <div class="form-group">
                                <label for="main_image">Imágen</label>
                                <div>
                                    <div class="file-loading">
                                        <input id="main_image" name="image" type="file">
                                    </div>
                                </div>
                            </div>
                            
                            <div id="kv-avatar-errors-2"></div>
                            <div class="form-group">
                                <button class="btn btn-primary float-right"
                                    id="save">Guardar nuevo logo</button>
                            </div>
                                          
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
@endsection
@section("js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection
@section("scripts")
    <script type="application/javascript">
        $(document).ready(function() {
            if (!$("#publish").is(':checked')) {
                $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
            }
            $("#publish").change( function(){
                if( $(this).is(':checked') ) {
                    $("#datetime").removeAttr("readOnly").datepicker("option", "disabled", false);
                }else{
                    $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
                }
            });
            var now = moment();
            let date = $("#datetime").val().length < 0 ? $("#datetime").val() : now.format('DD/MM/YYYY');
            $('#datetime').datepicker({
                changeMonth: true,
                changeYear: true,
            });

            $.datepicker.regional['es'];
        });
        
    </script>
    <script type="application/javascript">

        
        let functions = new Functions();
        $(document).ready(function () {
            $("#main_image").fileinput({
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                maxFileCount: 1,
                msgFileRequired: "Archivo requerido",
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-undo" id="rmain"></i>',
                uploadUrl: '{{url("logos/uploadFile")}}',
                required: true,
                removeTitle: 'Deshacer última acción',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
                   
            
        });

        function removeItemFromArr(arr, item) {
            let i = arr.indexOf( item );

            if ( i !== -1 ) {
                arr.splice( i, 1 );
            }
        }

        
    </script>
@endsection