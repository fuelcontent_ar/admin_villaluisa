<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true"
     style="z-index: 99999">
    <div class="modal-dialog modal-dialog-centered {{$size}}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titleModal">{{$title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" {{ $attributes->merge(['type' => 'modal']) }}>
                {{ $slot }}
            </div>
            @if(!$cancel || !$save || !$close)
                <div class="modal-footer">
                    @if($cancel)
                        <button type="reset" class="btn btn-secondary"
                                data-dismiss="modal">{{__("general.buttons.cancel")}}</button>
                    @endif
                    @if($save)
                        <button type="button" class="btn btn-primary" id="primary" onclick="execute()">
                            <span class="text-create">{{__("general.buttons.save")}}</span>
                            <span class="spinner-grow spinner-grow-sm hide" style="display: none" id="load"
                                  role="status" aria-hidden="true"></span>
                            <span class="sr-only hide" id="text-load">Loading...</span>
                        </button>
                    @endif
                    @if($close)
                        <button type="button" class="btn btn-primary"
                                data-dismiss="modal">{{__("general.buttons.close")}}</button>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>
