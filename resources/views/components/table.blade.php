<table id="table" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        @foreach($columns as $column)
            <th>{{__(sprintf("%s.labels.%s", $name, $column))}}</th>
        @endforeach
        <th>{{__("general.action")}}</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

@section("scripts")
    @parent
    <script>
        /** start datatables **/
        let columns = {!! $dataTablesColumns !!};
        columns.push({data: 'action', name: 'action', orderable: false, searchable: false});
        let config = {!! $config !!};
        $(document).ready(function () {
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{$route}}",
                "columns": columns,
                "columnDefs": config.style,
                "language": config.language
            });
        });
        /** end datatables **/
    </script>
@endsection
