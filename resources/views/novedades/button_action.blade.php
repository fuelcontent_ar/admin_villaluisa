<div>
    {{--<a type="button" class="btn btn-info btn-sm" href="{{route("novedades.show", $model->getAttribute("id"))}}"
            data-toggle="tooltip" data-placement="right" title="{{__("novedades.all.tooltip-show")}}">
        <i class="fa fa-eye"></i>
    </a>--}}
    <a class="btn btn-warning btn-sm" href="{{route("novedades.edit", $model->getAttribute("id"))}}"
       data-toggle="tooltip" data-placement="right" title="{{__("novedades.all.tooltip-edit")}}">
        <i class="fa fa-pencil"></i>
    </a>
    <button type="button" class="btn btn-danger btn-sm" onclick="drop({{$model->getAttribute("id")}})"
            data-toggle="tooltip" data-placement="right" title="{{__("novedades.all.tooltip-destroy")}}">
        <i class="fa fa-trash"></i>
    </button>
</div>
