@if(!$row->isAdmin())
    <div class="custom-control custom-switch" style="z-index: 0" data-placement="right"
         title="{{__("users.all.enable-disable")}}">
        <input type="checkbox"
               class="custom-control-input"
               id="active-{{$row->id}}"
               @if($row->active) checked @endif
               onchange="active({{$row->id}})"
        >
        <label class="custom-control-label" for="active-{{$row->id}}"></label>
    </div>
@endif
