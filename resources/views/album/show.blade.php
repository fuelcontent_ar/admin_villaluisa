@extends('layouts.app')

@section('content')
    <div class="container">
        <form
            action="{{isset($data) ? route("photos.update", $data["id"]) : route("photos.store")}}"
            method="POST"
            id="form"
            enctype="multipart/form-data"
        >
        <input type="hidden" name="path" value="{{$path}}">
        <input type="hidden" name="phototype_id" 
        value="
            @if(!empty($_GET["type"]))
            {{$_GET["type"]}}
            @elseif(isset($data))
            {{$data["phototype_id"]}}
            @else
            {{old("phototype_id")}}
            @endif
        ">               
            @isset($data)
                @method("PUT")
                <input type="hidden" name="file" value="{{$data["file"]}}">
                <input type="hidden" name="id" value="{{$data["id"]}}">
            @endisset
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <span id="message"></span>
                    <div class="card">
                        <div class="card-header">Subir nueva imágen</div>
                        <div class="card-body">
                            @if ($errors->any() || session()->has("message"))
                                @php
                                    $type = session()->has("message") ? "success" : "danger";
                                    $message = session()->has("message") ? session()->get("message") : __('validation.message');
                                @endphp
                                <x-alert :message="$message" :type="$type" :show="true" duration="100000"/>
                            @endif
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text"
                                       class="form-control"
                                       id="name"
                                       name="name"
                                       required
                                       placeholder="Nombre"
                                       value="{{ isset($data) ? $data["name"] : old("name") }}"
                                />
                            </div>
                            <div class="form-group">
                                <label for="name">Epígrafe</label>
                                <input type="text"
                                       class="form-control"
                                       id="epigraph"
                                       name="epigraph"
                                       
                                       placeholder="Epígrafe"
                                       value="{{ isset($data) ? $data["epigraph"] : old("epigraph") }}"
                                />
                            </div>
                           
                            <div class="form-group">
                                <label for="name">Orden</label>
                                <input type="text"
                                       class="form-control"
                                       id="order"
                                       name="order"
                                       required
                                       placeholder="{{isset($maximo) ? "El último nro es ".$maximo : "Nro de Orden"}}"
                                       value="{{ isset($data) ? $data["order"] : old("order") }}"
                                />
                            </div>
                                          
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4 card">
                        
                            <div class="form-group">
                                <label for="main_image">Imágen</label>
                                <div>
                                    <div class="file-loading">
                                        <input id="main_image" name="image" type="file">
                                    </div>
                                </div>
                            </div>

                            <div id="kv-avatar-errors-2"></div>
                            
                           
                            <div class="dropdown-divider"></div>
                            <div class="form-group">
                                <button class="btn btn-primary float-right"
                                    id="save">{{isset($data) ? __("general.buttons.edit") :__("general.buttons.save")}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection
@section("scripts")
    <script type="application/javascript">
        $(document).ready(function() {
            if (!$("#publish").is(':checked')) {
                $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
            }
            $("#publish").change( function(){
                if( $(this).is(':checked') ) {
                    $("#datetime").removeAttr("readOnly").datepicker("option", "disabled", false);
                }else{
                    $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
                }
            });
            var now = moment();
            let date = $("#datetime").val().length < 0 ? $("#datetime").val() : now.format('DD/MM/YYYY');
            $('#datetime').datepicker({
                changeMonth: true,
                changeYear: true,
            });

            $.datepicker.regional['es'];
        });
        
    </script>
    <script type="application/javascript">

        
        let functions = new Functions();
        $(document).ready(function () {
            $("#main_image").fileinput({
                @if(isset($data) || old("image"))
                    initialPreview: "{{ \Illuminate\Support\Facades\Storage::disk("public")->url($data["file"]) }}",
                    initialPreviewAsData: true,
                    deleteUrl: '{{url("photos/deletePicture", $data["file"])}}',
                    initialPreviewConfig:[
                        {
                            key: "{{ $data["file"] }}",
                        },
                    ],
                    ajaxDeleteSettings: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                @endif
                overwriteInitial: false,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                maxFileCount: 1,
                msgFileRequired: "Archivo requerido",
                browseIcon: " <i class='fa fa-folder-open-o'></i>",
                browseLabel: '',
                removeLabel: '',
                removeIcon: '<i class="fa fa-undo" id="rmain"></i>',
                uploadUrl: '{{url("photos/uploadFile",$path)}}',
                required: true,
                removeTitle: 'Deshacer última acción',
                elErrorContainer: '#kv-avatar-errors-2',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="{{asset("images/file-upload-image-icon.png")}}" class="image" alt="Subir imagen">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg",],
                ajaxSettings: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                previewZoomButtonIcons: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>',
                    toggleheader: '<i class="fa fa-arrows-h"></i>',
                    fullscreen: '<i class="fa fa-arrows-alt"></i>',
                    borderless: '<i class="fa fa-arrows"></i>',
                    close: '<i class="fa fa-times"></i>'
                },
                fileActionSettings: {
                    showUpload: false,
                    showRemove: false,
                    zoomIcon: "<i class='fa fa-search'></i>",
                    zoomTitle: "Ver imagen",
                    removeIcon: "<i class='fa fa-trash'></i>",
                    removeTitle: "Quitar imagen"
                }
            });
                   
            
        });

        function removeItemFromArr(arr, item) {
            let i = arr.indexOf( item );

            if ( i !== -1 ) {
                arr.splice( i, 1 );
            }
        }

        
    </script>
@endsection