@extends('layouts.app')

@section('content')
    <div class="container">
        <form
            action="{{isset($data) ? route("photos.update", $data["id"]) : route("photoTypes.store")}}"
            method="POST"
            id="form"
            enctype="multipart/form-data"
        >
        <input type="hidden" name="path" value="{{$path}}">
        <input type="hidden" name="phototype_id" value="@if(!empty($_GET["type"])) {{$_GET["type"]}}@endif">               
           
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <span id="message"></span>
                    <div class="card">
                        <div class="card-header">Agregar nueva categoría</div>
                        <div class="card-body">
                            @if ($errors->any() || session()->has("message"))
                                @php
                                    $type = session()->has("message") ? "success" : "danger";
                                    $message = session()->has("message") ? session()->get("message") : __('validation.message');
                                @endphp
                                <x-alert :message="$message" :type="$type" :show="true" duration="100000"/>
                            @endif
                            <div class="form-group">
                                <label for="name">Nombre en Español</label>
                                <input type="text"
                                       class="form-control"
                                       id="name"
                                       name="name"
                                       required
                                       placeholder="Nombre"
                                       value="{{ isset($data) ? $data["name"] : old("name") }}"
                                />
                            </div>
                            <div class="form-group">
                                <label for="name_en">Nombre en Inglés</label>
                                <input type="text"
                                       class="form-control"
                                       id="name_en"
                                       name="name_en"
                                       required
                                       placeholder="Nombre en inglés"
                                       value="{{ isset($data) ? $data["name_en"] : old("name_en") }}"
                                />
                            </div>
                            <div class="form-group">
                                <label for="name">Nombre en Francés</label>
                                <input type="text"
                                       class="form-control"
                                       id="name_fr"
                                       name="name_fr"
                                       required
                                       placeholder="Nombre en francés"
                                       value="{{ isset($data) ? $data["name_fr"] : old("name_fr") }}"
                                />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary float-right"
                                    id="save">{{isset($data) ? __("general.buttons.edit") :__("general.buttons.save")}}</button>
                            </div>
                                          
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
@endsection
@section("js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection
@section("scripts")
    <script type="application/javascript">
        $(document).ready(function() {
            if (!$("#publish").is(':checked')) {
                $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
            }
            $("#publish").change( function(){
                if( $(this).is(':checked') ) {
                    $("#datetime").removeAttr("readOnly").datepicker("option", "disabled", false);
                }else{
                    $("#datetime").attr("readOnly", "readOnly").datepicker("option", "disabled", true);
                }
            });
            var now = moment();
            let date = $("#datetime").val().length < 0 ? $("#datetime").val() : now.format('DD/MM/YYYY');
            $('#datetime').datepicker({
                changeMonth: true,
                changeYear: true,
            });

            $.datepicker.regional['es'];
        });
        
    </script>
    <script type="application/javascript">

        
        let functions = new Functions();
        
               

        function removeItemFromArr(arr, item) {
            let i = arr.indexOf( item );

            if ( i !== -1 ) {
                arr.splice( i, 1 );
            }
        }

        
    </script>
@endsection