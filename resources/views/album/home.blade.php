@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editor de fotos - Seleccione la categoría.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Estas son las categorías disponibles:</p>
                    <div class="container d-flex justify-content flex-wrap">
                    
                    
                    @foreach ($types as $type)
                        @if (in_array($type->id , $editables))
                        <div class="m-2">
                            <a href="{{route("photos.index",["type"=>$type->id])}}"", class="btn btn-primary">{{ucfirst($type->id_name)}}</a>
                        </div>
                        @else
                        <div class="m-2">
                            <a href="/photoTypes/search/{{$type->id}}" class="btn btn-primary">{{ucfirst($type->id_name)}}</a>
                        </div>
                        @endif
                    @endforeach
                    @if (in_array($types[0]->id , $editables))
                        <div class="m-2">
                            <a href="{{route("photoTypes.create",["type"=>$types[0]->parent_photo_type])}}"", class="btn btn-secondary">Agregar nueva categoría</a>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
