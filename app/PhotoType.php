<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoType extends Model
{
    protected $table = 'photo_types';
}
