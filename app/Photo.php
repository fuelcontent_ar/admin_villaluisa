<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        "name",
        "epigraph",
        "file",
        "order",
        "phototype_id",
        
    ];
}
