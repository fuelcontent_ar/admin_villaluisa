<?php

namespace App\Filters;

use App\Contracts\Models\FiltersInterface;

/**
 * Class ArticleFilters
 * @package App\Models\Filters
 */
class PhotoFilter extends Filters implements FiltersInterface
{
    /**
     * @var string[]
     */
    public $columnsFilter = [
        "name",
        "epigraph",
        "file",
        "order",
        "phototype_id",
        "from_creation_date",
        "to_creation_date",
    ];
}
