<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use WithScopeModel;

    /**
     * @var string[]
     */
    protected $fillable = ["key", "value", "user_id"];

    /**
     * @var string[]
     */
    protected $casts = [
        "created_at" => "datetime:d/m/Y",
        "value" => "array",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeUserId(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->whereHas("user", function ($query) use ($value) {
            return $query->whereId(intval($value));
        });
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeKey(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("key", "LIKE", "%".$value."%");
    }
}
