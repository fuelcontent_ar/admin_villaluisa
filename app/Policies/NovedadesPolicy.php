<?php

namespace App\Policies;

use App\Novedad;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class NovedadesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\novedad  $novedad
     * @return mixed
     */
    public function view(User $user, Novedad $novedad)
    {
        return $user->isAdmin() || $user->id === $novedad->user_id
            ? Response::allow()
            : redirect()->back()->with([
                "error" => __("general.unauthorized.view")
            ]);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {

    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\novedad  $novedad
     * @return mixed
     */
    public function update(User $user, Novedad $novedad)
    {
        return $user->isAdmin() || $user->id === $novedad->user_id
            ? Response::allow()
            : redirect()->back()->with([
                "error" => __("general.unauthorized.view")
            ]);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\novedad  $novedad
     * @return mixed
     */
    public function delete(User $user, Novedad $novedad)
    {
        return $user->isAdmin() || $user->id === $novedad->user_id
            ? Response::allow()
            : redirect()->back()->with([
                "error" => __("general.unauthorized.view")
            ]);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\novedad  $novedad
     * @return mixed
     */
    public function restore(User $user, Novedad $novedad)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\novedad  $novedad
     * @return mixed
     */
    public function forceDelete(User $user, Novedad $novedad)
    {
        //
    }
}
