<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes, WithScopeModel;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'active' => 'bool',
        'last_login' => 'datetime:d/m/Y H:i:s'
    ];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes["password"] = Hash::make($value);
    }

    /**
     * @param $value
     */
    public function setActiveAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes["active"] = $value === "on" ? 1 : 0;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function article()
    {
        return $this->hasMany(Article::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documents()
    {
        return $this->belongsTo(Document::class);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeName(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("name", "LIKE", "%".$value."%");
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeEmail(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("email", $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeActive(Builder $query, $value): Builder
    {
        return empty($value) ? $query : $query->where("active", $value);
    }
}
