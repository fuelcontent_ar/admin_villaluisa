<?php


namespace App\Repositories;

use App\Photo;
use App\Filters\PhotoFilter;
use App\Filters\Filters;

class PhotoRepository extends Repository
{
    

    public function getByType($type)
    {
        return $this->getFilter()->apply($this->withModel()->where('phototype_id', $type));
    }
    
    
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, Photo::class)) {
            $this->model = app(Photo::class);
        }
        return $this->model;
    }



    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, PhotoFilter::class)) {
            $this->filter = new PhotoFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query();
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return [];
    }

    
}
