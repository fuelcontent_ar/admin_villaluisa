<?php


namespace App\Repositories;

use App\Novedad;
use App\Filters\NovedadFilter;
use App\Filters\Filters;

class NovedadRepository extends Repository
{
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, Novedad::class)) {
            $this->model = app(Novedad::class);
        }
        return $this->model;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, NovedadFilter::class)) {
            $this->filter = new NovedadFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query();
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return [];
    }
}
