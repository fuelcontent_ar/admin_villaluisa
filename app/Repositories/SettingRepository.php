<?php


namespace App\Repositories;

use App\Filters\Filters;
use App\Filters\SettingFilter;
use App\Setting;

class SettingRepository extends Repository
{
    /**
     * {@inheritDoc}
     */
    protected function getModel()
    {
        if (!is_a($this->model, Setting::class)) {
            $this->model = app(Setting::class);
        }
        return $this->model;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilter(): Filters
    {
        if (!is_a($this->filter, SettingFilter::class)) {
            $this->filter = new SettingFilter($this->request);
        }

        return $this->filter;
    }

    /**
     * {@inheritDoc}
     */
    protected function withModel()
    {
        return $this->getModel()->query()->with("user");
    }

    /**
     * {@inheritDoc}
     */
    protected function extraData(): array
    {
        return [];
    }
}
