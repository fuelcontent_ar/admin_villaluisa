<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NovedadCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => ["required", "string", "max:100",],
            "slug" => ["required", "string", "max:100",],
            "description" => ["required","string","max:300"],
            "lang" => "required|string|max:3",
            "published" => [Rule::in(['on', 'off']),],
            "published_at" => 'exclude_unless:published,"on"|required',
            "image" => ["required", "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=550,max_width=650,min_height=300,max_height=400"],
        ];
    }

    public function messages()
    {
        return [
            "image.required" => "La Imagen principal es requerida.",
            "published_at.required" => "Es necesario incluir una fecha de publicación",
            "image.dimensions" => " Las dimensiones de la imágen principal deben ser: Ancho entre 550 y 650, Alto entre 300 y 400",
                        
        ];
    }

    public function attributes()
    {
        return [
                     
            "title" => __("novedades.labels.title"),
            "description" => __("novedades.labels.description"),
            "lang" => __("novedades.labels.lang"),
            
        ];
    }
}
