<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LogoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required", "string", "max:30",],
            "image" => ["required", "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=500,max_width=600,min_height=200,max_height=300"],
        ];
    }

    public function messages()
    {
        return [
            "image.required" => "La Imagen principal es requerida.",
            "image.dimensions" => " Las dimensiones de la imágen principal deben ser: Ancho entre 500 y 600, Alto entre 200 y 300",
        ];
    }

    public function attributes()
    {
        return [
                     
            "title" =>"Imágen",
            "name" => "Nombre",
                        
        ];
    }
}
