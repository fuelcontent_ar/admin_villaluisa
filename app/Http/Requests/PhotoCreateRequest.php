<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PhotoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required", "string", "max:100",],
            "epigraph" => ["required", "string", "max:20",],
            "order" => ["required","int"],
            "path" => ["required","string","max:300"],
            "phototype_id" => ["required","int"],
            "image" => ["required", "image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=890,max_width=1500,min_height=650,max_height=1500"],
        ];
    }

    public function messages()
    {
        return [
            "image.required" => "La Imagen principal es requerida.",
            "image.dimensions" => " Las dimensiones de la imágen principal deben ser: Ancho entre 900 y 1500, Alto entre 650 y 1500",
                                
        ];
    }

    public function attributes()
    {
        return [
                     
            "title" => __("novedades.labels.title"),
            "description" => __("novedades.labels.description"),
            "lang" => __("novedades.labels.lang"),
            
        ];
    }
}
