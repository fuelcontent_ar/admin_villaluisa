<?php

namespace App\Http\Requests;

use App\Rules\EmptyArray;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Storage;

class NovedadUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            "title" => ["string", "max:100",],
            "slug" => ["string", "max:100",],
            "description" => ["required","string","max:300"],
            "lang" => "string|max:3",
            "published" => [Rule::in(['on', 'off']),],
            "published_at" => 'exclude_unless:published,"on"|required',
            "image" => ["image", "mimes:jpg,jpeg,png", "max:900", "dimensions:min_width=550,max_width=650,min_height=300,max_height=400"],
        ];
    }

    public function messages()
    {
        return [
           
            "image.required" => "La Imagen principal es requerida.",
            "image.dimensions" => " Las dimensiones de la imágen principal deben ser: Ancho entre 550 y 650, Alto entre 300 y 400",
                
           
        ];
    }

    public function attributes()
    {
        return [
            "title" => __("novedades.labels.title"),
            "description" => __("novedades.labels.description"),
            "lang" => __("novedades.labels.lang"),
            "published_at" => "'Publicada En'"
            
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            if (!Storage::disk('novedades')->exists($this->input("image_url"))) {
                if(!$this->hasFile("image")){
                      
                $validator->errors()->add('field', 'Por favor debe ingresar una foto principal');
                
                }
            }
        });
    }
}
