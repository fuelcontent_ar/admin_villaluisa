<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required", "min:3", "max:255"],
            "email" => ["required", "email", "unique:users,email"],
            "active" => ["string", "max:3"],
            "roles" => ["required"],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            "name" => __("users.labels.name"),
            "email" => __("users.labels.email"),
            "active" => __("users.labels.active"),
        ];
    }
}
