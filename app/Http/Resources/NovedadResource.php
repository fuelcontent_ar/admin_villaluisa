<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NovedadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->getAttribute("id"),
            "title" => $this->getAttribute("title"),
            "slug" => $this->getAttribute("slug"),
            "description" => $this->getAttribute("description"),
            
            "published" => $this->getAttribute("published"),
             "published_at" => !empty($this->getAttribute("published_at"))
                ? $this->getAttribute("published_at")->format("d/m/Y")
                : null,
            "user_id" => $this->user,
            "image_url" => $this->getAttribute("image_url"),
            "thumb_url" => $this->getAttribute("thumb_url"),
            "lang" => $this->getAttribute('lang'),
            
        ];
    }
}
