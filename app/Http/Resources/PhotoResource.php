<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->getAttribute("id"),
            "name" => $this->getAttribute("name"),
            "epigraph" => $this->getAttribute("epigraph"),
            "file" => $this->getAttribute("file"),
            "order" => $this->getAttribute("order"),
            "phototype_id" => $this->getAttribute("phototype_id"),
                        
        ];
    }
}
