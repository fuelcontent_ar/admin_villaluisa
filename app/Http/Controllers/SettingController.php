<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use App\Http\Resources\SettingResource;
use App\Managers\SettingManager;
use App\Repositories\SettingRepository;
use App\Setting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SettingRepository $repository
     * @param SettingManager $manager
     * @return \Illuminate\Contracts\View\View
     */
    public function index(SettingRepository $repository, SettingManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }
        return View::make("settings.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("settings.index"),
            "config" => $manager->datatableConfig(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make("settings.index", [
            "title" => "save",
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SettingRequest $request
     * @param SettingRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SettingRequest $request, SettingRepository $repository)
    {
        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
        ]);
        $create = new SettingResource(
            $repository->create($request->only("key",  "user_id", "value")));
        return redirect()->back()->with([
            "message" => __("settings.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Setting $setting)
    {
        return View::make("settings.index", [
            "title" => "edit",
            "data" => (new SettingResource($setting))->toArray(\request()),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SettingRequest $request
     * @param \App\Setting $setting
     * @param SettingRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SettingRequest $request, Setting $setting, SettingRepository $repository)
    {
        $repository->update($setting, $request->only(["key", "value"]));
        return redirect()->back()->with([
            "message" => __("settings.edit.message"),
            "data" => (new SettingResource($setting))->toArray($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Setting $setting
     * @param SettingRepository $repository
     * @return JsonResponse
     */
    public function destroy(Setting $setting, SettingRepository $repository)
    {
        $repository->destroy($setting);
        return JsonResponse::fromJsonString(__("settings.controllers.destroy"));
    }
}
