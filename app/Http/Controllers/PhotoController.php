<?php

namespace App\Http\Controllers;


use App\Photo;
use App\PhotoType;
use App\Http\Resources\PhotoResource;
use App\Managers\PhotoManager;
use App\Http\Requests\PhotoCreateRequest;
use App\Http\Requests\PhotoUpdateRequest;
use App\Http\Requests\PhotoUploadRequest;
use App\Repositories\PhotoRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(Request $request, PhotoManager $manager, PhotoRepository $repository)
    {
          
        if (\request()->wantsJson()) {
             
            $url = redirect()->getUrlGenerator()->previous();
            $type = substr($url, strpos($url,'type=') + 5, strlen($url) -  strpos($url,'type=') + 5);
                        
            return $manager->datatable($repository->getByType($type));
            
        }
              
        return  View::make("album.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("photos.index"),
            "config" => $manager->datatableConfig(),
            
        ]);
             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo=$_GET['type'];
        
        $ordenMaximo = Photo::where('phototype_id',$tipo)->pluck('order')->max();

        $path = PhotoType::where('id',$tipo)->get('path')->first()['path'];
                
        return View::make("album.show", [
            "title" => "save",
            "language" => config('general.language'),
            "maximo" => $ordenMaximo,
            "path" => $path,
            
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoCreateRequest $request, PhotoRepository $repository)
    {              
                     
        $uploadedImages = $this->upload($request);
        $path = $request->input('path');
        $file = $path . '/' .$uploadedImages['principal_image'];
            
            $request->request->add([
                "user_id" => auth()->user()->getAuthIdentifier(),
                "file" => $file ,
            ]);
                        
    
            $create = new PhotoResource(
                $repository->create($request->only(
                    "file",
                    "name",
                    "epigraph",
                    "order",
                    "phototype_id"
                )
           ));
            return redirect()->back()->with([
                "message" => "Foto guardada con éxito",
                "data" => $create->toArray($request)
            ]);
              
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
        {
            Gate::authorize("view", $photo);
            
            $datos = new PhotoResource($photo);
            $tipo = $datos->phototype_id;
            $path = PhotoType::where('id',$tipo)->get('path')->first()['path'];
            
            return View::make("album.show", [
                "title" => "edit",
                "data" => (new PhotoResource($photo))->toArray(\request()),
                "language" => config('general.language'),
                "path" => $path,
                "type"=>$tipo
                
            ]);
            
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhotoUpdateRequest $request, Photo $photo, PhotoRepository $repository)
    {
        
        Gate::authorize("update", $photo);
                
        $updatedImages = $this->upload($request);
        $path = $request->input('path');
        $newFile = $path . '/' .$updatedImages['principal_image'];
                      
        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "file" => $request->hasFile('image') ? $newFile : $request->input('file'),
        ]);
         
        $repository->update($photo, $request->only(
            "file",
            "name",
            "epigraph",
            "order",
            "phototype_id"
        ));
    
        return redirect()->back()->with([
            "message" => __("novedades.edit.message"),
            "data" => (new PhotoResource($photo))->toArray($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo, PhotoRepository $repository)
    {
        Gate::authorize('delete', $photo);
        $repository->destroy($photo);
        return JsonResponse::fromJsonString("Foto eliminada con éxito");
    }

    public function uploadFile(PhotoUploadRequest $request)
    {
        $fileName = $this->upload($request);
        return response()->json([
            "type" => "image",
            "success" => 1,
            "file" => [
                "url" => Storage::url("photos/" . $fileName),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request)
    {
        
        $fileName = $request->has("image") ? $request->input("image") : $request->input("key");
        
        if (Storage::exists("photos/".$fileName)) {
            Storage::delete("photos/".$fileName);
        }
        if ($request->has("key")) {
            return response()->json([
               $request->all()
            ]);
        }
    }

    public function deletePicture(Request $request, $route)
    {
        
        $route = str_replace('%2F','/',$route);
                       
        if (Storage::exists($route)) {
            Storage::delete($route);
        }
        if ($request->has("key")) {
            return response()->json([
               $request->all()
            ]);
        }
    }

    private function upload(Request $request)
    {      
       
        $principalImageName = "";
        $path = $request->input('path');              
        
        if ($request->hasFile("image")) {
             
            $principal_image = $request->file("image");
            $principalImageName = Str::uuid() . "." . $principal_image->getClientOriginalExtension();
            $principal_image->storeAs($path, $principalImageName);     
        } 
       
        
        $fileNames = array(
            "principal_image" => $principalImageName
        );
        
        return $fileNames;
    }
}
