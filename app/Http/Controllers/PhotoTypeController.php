<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PhotoType;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class PhotoTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null)
    {
            
      if($type == null){
        $types = Phototype::whereNull('parent_photo_type')
        ->get(['id','id_name','parent_photo_type']);
         
      }
      else {
        $types = Phototype::where('parent_photo_type',$type)
      ->get(['id','id_name','parent_photo_type']);
       
      } 
            
      $parentTypes = Phototype::whereNotNull('parent_photo_type')
      ->groupBy('parent_photo_type')
      ->pluck('parent_photo_type')
      ->toArray();

      $editableTypes = 
      Phototype::whereNotIn('id', $parentTypes)
      //->where('id', '>=', 11)
      ->pluck('id')
      ->toArray();
      
      return view('album.home',[ 'editables' => $editableTypes , 'types'=>$types]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                
        $tipo=$_GET['type'];
                
        $path = PhotoType::where('id',$tipo)->get('path')->first()['path'];
         
        return View::make("album.newType", [
            "title" => "save",
            "language" => config('general.language'),
            "path" => $path,
            
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Phototype $phototype)
    {
        $nombre_es = $request->input('name');
        $nombre_en = $request->input('name_en');
        $nombre_fr = $request->input('name_fr');
        $rutaRaiz = $request->input('path');
        $parentType = $request->input('phototype_id');
        
        $nombreCarpeta = strtolower(str_replace(" ", "_",$nombre_es));

        $nombreRuta = $rutaRaiz."/".$nombreCarpeta;

        $result = Storage::disk('public')->makeDirectory($nombreRuta);
        
        $phototype->id_name = $nombre_es;
        $phototype->name = $nombre_es;
        $phototype->name_en = $nombre_en;
        $phototype->name_fr = $nombre_fr;
        $phototype->parent_photo_type= $parentType;
        $phototype->path = $nombreRuta;

        $phototype->save();

        return redirect()->back()->with([
            "message" => "Categoría guardada con éxito",
            
        ]);


    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
