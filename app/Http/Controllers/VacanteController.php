<?php

namespace App\Http\Controllers;


use App\Vacante;
use App\Http\Requests\VacanteCreateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class VacanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(Request $request)
    {
          
        $vacantes = Vacante::paginate(10);
                           
        return view("vacantes.all", ['vacantes'=>$vacantes]);
             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {                        
        return view('vacantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Vacante $vacante)
    {              
                                          
        $name = $request->input('title');
        $text = $request->input('descripcion');
        
        $vacante->title = $name;
        $vacante->text = $text;

        $vacante->save();
                
            return redirect()->back()->with([
                "message" => "Vacante guardada con éxito"
            ]);
              
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacante $vacante)
        {
            return view('vacantes.show', ["vacante"=>$vacante]);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $vacante = Vacante::find($id);

        $vacante->title = $request->input('title');
        $vacante->text = $request->input('descripcion');
        
        if($vacante->isDirty()){
            $vacante->save();
        }
        return redirect()->back()->with([
            "message" => "Vacante editada con éxito"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacante $vacante, Request $request )
    {
        $id = str_replace("vacantes/","",$request->path());
        Vacante::where('id', $id)->delete();
        
        return JsonResponse::fromJsonString("Vacante eliminada con éxito");
    }

    
}
