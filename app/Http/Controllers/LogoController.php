<?php

namespace App\Http\Controllers;


use App\Logo;
use App\Http\Requests\LogoCreateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(Request $request)
    {
          
        $logos = Logo::paginate(10);
                           
        return view("logos.all", ['logos'=>$logos]);
             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {                        
        return view('logos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LogoCreateRequest $request, Logo $logo)
    {              
                    
        $uploadedImages = $this->upload($request);
                      
        $name = $request->input('name');
        $file = "/storage/logos/".$uploadedImages;
        $logo->name = $name;
        $logo->file = $file;

        $logo->save();
                
            return redirect()->back()->with([
                "message" => "Logo guardado con éxito"
            ]);
              
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
        {
            
            
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($request)
    {
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logo $logo, Request $request )
    {
        $id = str_replace("logos/","",$request->path());
        Logo::where('id', $id)->delete();
        redirect()->to('/logos'); 
        return JsonResponse::fromJsonString("Logo eliminado con éxito");
    }

    public function uploadFile(Request $request)
    {
        $fileName = $this->upload($request);
        return response()->json([
            "type" => "image",
            "success" => 1,
            "file" => [
                "url" => Storage::url("logos/" . $fileName),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request)
    {
        
        $fileName = $request->has("image") ? $request->input("image") : $request->input("key");
        
        if (Storage::exists("logos/".$fileName)) {
            Storage::delete("logos/".$fileName);
        }
        if ($request->has("key")) {
            return response()->json([
               $request->all()
            ]);
        }
    }

    public function deletePicture(Request $request, $route)
    {
        
        $route = str_replace('%2F','/',$route);
                       
        if (Storage::exists($route)) {
            Storage::delete($route);
        }
        if ($request->has("key")) {
            return response()->json([
               $request->all()
            ]);
        }
    }

    private function upload(Request $request)
    {      
       
        $principalImageName = "";
               
        if ($request->hasFile("image")) {
             
            $principal_image = $request->file("image");
            $principalImageName = Str::uuid() . "." . $principal_image->getClientOriginalExtension();
            $principal_image->storeAs("logos", $principalImageName, "public");     
        } 
                       
        return $principalImageName;
    }
}
