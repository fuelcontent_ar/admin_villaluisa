<?php

namespace App\Http\Controllers;

use App\Novedad;
use App\Blog;
use App\Category;
use App\Http\Requests\NovedadCreateRequest;
use App\Http\Requests\NovedadUpdateRequest;
use App\Http\Requests\NovedadUploadRequest;
use App\Http\Resources\NovedadResource;
use App\Managers\NovedadManager;
use App\Repositories\NovedadRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;

class NovedadController extends Controller
{

    /**
     * @var Blog
     */
    private $model;

    /**
     * Display a listing of the resource.
     *
     * @param NovedadRepository $repository
     * @param NovedadManager $manager
     * @return \Illuminate\Contracts\View\View
     */
    public function index(NovedadRepository $repository, NovedadManager $manager)
    {
        if (\request()->wantsJson()) {
            return $manager->datatable($repository->all());
        }
        
        return View::make("novedades.all", [
            "columns" => $manager->columns(),
            "dataTablesColumns" => $manager->dataTablesColumns(),
            "route" => route("novedades.index"),
            "config" => $manager->datatableConfig(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        
        return View::make("novedades.show", [
            "title" => "save",
            "language" => config('general.language'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NovedadCreateRequest $request, NovedadRepository $repository)
    {
        if (!$request->has("published")) {
            $request->request->set("published_at", null);
        }
        
        $uploadedImages = $this->upload($request);
        $thumbUrl = $this->uploadThumb($request->file('image'));

        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "image_url" => $uploadedImages['principal_image'],
            "thumb_url" => $thumbUrl,
            "published" => $request->has("published") ? "on" : "off",
            "highlighted" => $request->has("highlighted") ? "on" : "off",
        ]);
        //$this->renameFile($request);
        

        $create = new NovedadResource(
            $repository->create($request->only(
                "title",
                "slug",
                "description",
                "lang",
                "published",
                "published_at",
                "user_id",
                "image_url",
                "thumb_url"
            )
        ));
        return redirect()->back()->with([
            "message" => __("novedades.save.message"),
            "data" => $create->toArray($request)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Novedad $Novedad
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Novedad $novedad)
    {
       
        Gate::authorize("view", $novedad);
                
        return View::make("novedades.show", [
            "title" => "edit",
            "data" => (new NovedadResource($novedad))->toArray(\request()),
            "language" => config('general.language'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NovedadUpdateRequest $request
     * @param \App\Novedad $Novedad
     * @param NovedadRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NovedadUpdateRequest $request, Novedad $novedad, NovedadRepository $repository)
    {       
        
        Gate::authorize("update", $novedad);
        if (is_null($request->input("published"))) {
            $request->request->set("published_at", null);
        }
        
        $updatedImages = $this->upload($request);
               
        $request->request->add([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "image_url" => $request->hasFile("image") ? $updatedImages['principal_image'] : ( Storage::disk('novedades')->exists($request->input("image_url")) ? $request->input("image_url") : $updatedImages['principal_image'])   ,
            "thumb_url" => $request->hasFile("image") ? $this->uploadThumb($request->file('image')) : $request->input("thumb_url"),
            "highlighted" => $request->has("highlighted") ? "on" : "off",
            "published" => $request->has("published") ? "on" : "off",
                   
        ]);
        $this->renameFile($request);
           
        $repository->update($novedad, $request->only(
            "title",
            "slug",
            "description",
            "lang",
            "published",
            "published_at",
            "highlighted",
            "blog_id",
            "user_id",
            "image_url",
            "thumb_url"
        ));
        

        return redirect()->back()->with([
            "message" => __("novedades.edit.message"),
            "data" => (new NovedadResource($novedad))->toArray($request)
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Novedad $Novedad
     * @return JsonResponse
     */
    public function destroy(Novedad $novedad, NovedadRepository $repository)
    {
        Gate::authorize('delete', $novedad);
        $repository->destroy($novedad);
        return JsonResponse::fromJsonString(__("documents.controllers.destroy"));
    }

    /**
     * @param NovedadUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadFile(NovedadUploadRequest $request)
    {
        $fileName = $this->upload($request);
        return response()->json([
            "type" => "image",
            "success" => 1,
            "file" => [
                "url" => Storage::url("novedades/" . $fileName),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request)
    {
        $fileName = $request->has("image") ? $request->input("image") : $request->input("key");
        if (Storage::exists("novedades/".$fileName)) {
            Storage::delete("novedades/".$fileName);
        }
        if ($request->has("key")) {
            return response()->json([
               $request->all()
            ]);
        }
    }
  

    /**
     * @param Request $request
     * @return string|string[]
     */
    private function upload(Request $request)
    {
        
        $principalImageName = "";
            
        if ($request->hasFile("image")) {
            
            $principal_image = $request->file("image");
            $principalImageName = Str::uuid() . "." . $principal_image->getClientOriginalExtension();
            $principal_image->storeAs("novedades", $principalImageName, "public");     
        } 
      
        $fileNames = array(
            
            "principal_image" => $principalImageName
        );
        
        return $fileNames;
    }

    /**
     * @param Request $request
     */
    private function renameFile(Request $request)
    {
        $path = "novedades";
        if ($request->has("images") && !empty($request->input("images"))) {
            $images = [];
            foreach (json_decode($request->input("images"), true) as $image) {
                if (Storage::exists($path. "/". $image)) {
                    $extension = explode(".", $image);
                    $fileName = Str::uuid().".".end($extension);
                    Storage::move($path. "/". $image, $path. "/". $fileName);
                    $images[] = $fileName;
                }
            }
            $request->request->set("images", json_encode($images));
        }
    }

    private function uploadThumb($image)
    {
        
        $ImgManager = new ImageManager();
        $thumbName = Str::uuid().".".$image->getClientOriginalExtension();
        $thumb = $ImgManager
        ->make($image->getRealPath())
        ->heighten(213)
        ->crop(213,213)
        ->save("storage/novedades/".$thumbName);
     
        return $thumbName;    
    }
}
