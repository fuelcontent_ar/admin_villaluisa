<?php


namespace App\Managers;


use Illuminate\Support\Facades\View;
use Yajra\DataTables\EloquentDataTable;

class PhotoManager extends Manager
{

    /**
     * {@inheritDoc}
     */
    protected function notOrder(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function notFind(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function addColumnsTable(EloquentDataTable $dataTables): EloquentDataTable
    {
        
        return $dataTables;
    }

    /**
     * {@inheritDoc}
     */
    public function columns(): array
    {
        return [
            'name', 'epigraph', 'file', 'order', 'phototype_id'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function datatableConfig(): string
    {
        return json_encode([
            "style" => [
                ["targets" => 0, "width" => "30%",],
                ["targets" => 1, "width" => "25%",],
                ["targets" => 2, "className" => "text-center", "width" => "14%",],
            ],
            "language" => [
                "url" => __("novedades.all.lang-datatables"),
            ]
           
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function viewActionButtons($model)
    {
        return View::make("album.button_action", ["model" => $model]);
    }
}
