<?php


namespace App\Managers;


use Illuminate\Support\Facades\View;
use Yajra\DataTables\EloquentDataTable;

class UserManager extends Manager
{
    /**
     * @var string[]
     */
    protected $addColumns = ["active"];

    /**
     * {@inheritDoc}
     */
    protected function notOrder(): array
    {
        return ["active",];
    }

    /**
     * {@inheritDoc}
     */
    protected function notFind(): array
    {
        return ["active",];
    }

    /**
     * {@inheritDoc}
     */
    protected function addColumnsTable(EloquentDataTable $dataTables): EloquentDataTable
    {
        return $dataTables->addColumn('active', function($row){
            return View::make("users.switch", ["row" => $row]);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function columns(): array
    {
        return [
            'name', 'email', 'active', 'last_login'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function datatableConfig(): string
    {
        return json_encode([
            "style" => [
                ["targets" => 2, "className" => "text-center",],
            ],
            "language" => [
                "url" => __("users.all.lang-datatables"),
            ]
        ]);
    }

    /**
     * @param $model
     * @return mixed
     */
    protected function viewActionButtons($model)
    {
        return View::make("users.button_action", ["model" => $model]);
    }
}
