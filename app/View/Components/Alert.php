<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $vertical;
    /**
     * @var string
     */
    private $horizontal;
    /**
     * @var int|null
     */
    private $duration;

    /**
     * @var bool
     */
    private $show;

    /**
     * Create a new component instance.
     *
     * @param string $type
     * @param string|null $message
     * @param bool $show
     * @param mixed $vertical
     * @param mixed $horizontal
     * @param int|null $duration
     */
    public function __construct(
        string $type,
        $message = null,
        bool $show = false,
        string $vertical = null,
        string $horizontal = null,
        int $duration = 3000
    ) {
        $this->message = $message;
        $this->type = $type;
        $this->vertical = $vertical;
        $this->horizontal = $horizontal;
        $this->duration = $duration;
        $this->show = $show;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.alert', [
            "message" => $this->message,
            "type" => $this->type,
            "style" => $this->style(),
            "duration" => $this->duration,
            "show" => $this->show,
        ]);
    }

    protected function style()
    {
        if ($this->vertical == "top" && $this->horizontal == "right") {
            return "right: 1%; z-index: 999; top: 14%;height: 50px;";
        }

        return null;
    }
}
