<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Modal extends Component
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var bool
     */
    private $cancel;
    /**
     * @var bool
     */
    private $save;
    /**
     * @var bool
     */
    private $close;

    /**
     * @var string
     */
    private $size;

    /**
     * Create a new component instance.
     *
     * @param string $title
     * @param string $size
     * @param bool $cancel
     * @param bool $save
     * @param bool $close
     */
    public function __construct(
        string $title,
        string $size = '',
        bool $cancel = false,
        bool $save = false,
        bool $close = false
    ) {
        $this->title = $title;
        $this->cancel = $cancel;
        $this->save = $save;
        $this->close = $close;
        $this->size = $size;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.modal', [
            "title" => $this->title,
            "cancel" => $this->cancel,
            "save" => $this->save,
            "close" => $this->close,
            "size" => $this->size,
        ]);
    }
}
