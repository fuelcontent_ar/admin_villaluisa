<?php

namespace App\Providers;

use App\View\Components\Alert;
use App\View\Components\Modal;
use App\View\Components\Pagination;
use App\View\Components\Table;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ComponentProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component(Table::class, 'table');
        Blade::component(Alert::class, 'alert');
        Blade::component(Modal::class, 'modal');
    }
}
