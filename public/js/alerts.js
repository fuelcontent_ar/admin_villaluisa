class Alerts {
    warning(title, message, text_button, func = function (){}) {
        swal({
            title: title,
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: text_button.yes,
            cancelButtonText: text_button.no,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },func)
    };
}
