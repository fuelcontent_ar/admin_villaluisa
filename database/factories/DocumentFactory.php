<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        "title" => $faker->sentence,
        "document_url" => "storage/documents/dummy.pdf",
        "image_url" => "storage/documents/test.jpg",
        "lang" => $faker->randomElement(["es", "en", "pt"]),
        "highlighted" => $faker->boolean(30) ? "on" : "off",
        "document_type_id" => \App\DocumentType::query()->inRandomOrder()->first()->getAttribute("id"),
        "user_id" => \App\User::query()->inRandomOrder()->first()->getAttribute("id"),
    ];
});
