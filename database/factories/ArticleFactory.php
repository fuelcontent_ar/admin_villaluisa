<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {

    $fakeContent = [];
    for($i = 0; $i < $faker->numberBetween(2, 8); $i++){
        $block = $faker->numberBetween(1, 8);
        switch($block){
            case 1:
                array_push($fakeContent, [
                    "type"=> "paragraph",
                    "data"=> [
                       "text"=> $faker->sentence($faker->numberBetween(10, 45), true)
                    ]
                ]);
            break;
            case 2:
                array_push($fakeContent, [
                    "type"=> "header",
                    "data"=> [
                       "text"=> $faker->sentence($faker->numberBetween(1, 5), true),
                       "level"=> $faker->numberBetween(2, 4)
                    ]
                ]);
            break;
            case 3:
                $listItems = $faker->numberBetween(3, 9);
                $baselistlen = $faker->numberBetween(2, 10);
                $blocktoadd = [
                    "type"=> "list",
                    "data"=> [
                       "style"=> $faker->numberBetween(1, 2) == 1 ? "unordered" : "ordered",
                       "items"=> []
                    ]
                ];
                for($i = 0; $i < $listItems; $i++) {
                    array_push($blocktoadd["data"]["items"], $faker->sentence($faker->numberBetween($baselistlen+1, $baselistlen+10), true));
                }
                array_push($fakeContent, $blocktoadd);
            case 4:
                array_push($fakeContent, [
                    "type"=> "delimiter",
                    "data"=> []
                ]);
            case 5:
                $imgwidth = $faker->numberBetween(500, 1000);
                $omgheight = $faker->numberBetween(500, 1000);
                array_push($fakeContent, [
                    "type"=> "image",
                    "data"=> [
                        "file" => [
                            "url" => "/storage/articles/".$faker->randomElement(["foto1.jpg", "foto2.jpg", "foto3.jpg"])
                        ],
                        "caption" => $faker->sentence(3, 10),
                        "withBorder" => $faker->boolean(30),
                        "stretched" => $faker->boolean(50),
                        "withBackground" => false
                    ]
                ]);
            case 6:
                $rows = $faker->numberBetween(1, 15);
                $cols = $faker->numberBetween(2, 4);
                $tableData = [];
                for($i = 0; $i < $rows; $i++){
                    $lastindex = array_push($tableData, []) - 1;
                    for($j = 0; $j < $cols; $j++){
                        array_push($tableData[$lastindex], $faker->sentence(1, 3));
                    }
                }
                array_push($fakeContent, [
                    "type"=> "table",
                    "data"=> [
                        "content" => $tableData
                    ]
                ]);
            case 7:
                $imgwidth = $faker->numberBetween(500, 1000);
                $omgheight = $faker->numberBetween(500, 1000);
                array_push($fakeContent, [
                    "type"=> "twoColumns",
                    "data"=> [
                        "fileLeft" => [
                            "url" => "/storage/articles/".$faker->randomElement(["foto1.jpg", "foto2.jpg", "foto3.jpg"])
                        ],
                        "fileRight" => [
                            "url" => "/storage/articles/".$faker->randomElement(["foto1.jpg", "foto2.jpg", "foto3.jpg"])
                        ],
                        "captionLeft" => $faker->sentence(3, 10),
                        "captionRight" => $faker->sentence(3, 10),
                        "alternate" => $faker->boolean(40),
                    ]
                ]);
            case 8:
                $imgwidth = $faker->numberBetween(500, 1000);
                $omgheight = $faker->numberBetween(500, 1000);
                array_push($fakeContent, [
                    "type"=> "quote",
                    "data"=> [
                        "file" => [
                            "url" => "/storage/articles/".$faker->randomElement(["foto1.jpg", "foto2.jpg", "foto3.jpg"])
                        ],
                        "caption" => $faker->sentence(3, 10),
                        "captionQuote" => $faker->sentence(18, 10),
                    ]
                ]);
        }
    }
    $imgwidth = $faker->numberBetween(500, 1000);
    $omgheight = $faker->numberBetween(500, 1000);
    $title = $faker->sentence;
    return [
        "title" => $title,
        "slug" => Str::slug($title),
        "description" => $faker->sentence,
        "body" => json_encode([
            "time" => "1234567890",
            "blocks" => $fakeContent,
            "version" => "2.19.0"
        ]),
        "images" => null,
        "lang" => $faker->randomElement(["es", "en", "pt"]),
        "video_url" => $faker->boolean(20) ? "https://www.youtube.com/watch?v=BQCKm8lBTI4" : "",
        "image_url" => $faker->randomElement(["foto1.jpg", "foto2.jpg", "foto3.jpg"]),
        "published" => $faker->boolean(80) ? "on" : "off",
        "highlighted" => $faker->boolean(30) ? "on" : "off",
        "published_at" => now()->format("d/m/Y"),
        "category_id" => \App\Category::query()->inRandomOrder()->first()->getAttribute("id"),
        "user_id" => \App\User::query()->inRandomOrder()->first()->getAttribute("id"),
    ];
});
