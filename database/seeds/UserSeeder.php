<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => "admin",
                'email' => "admin@adecoagro.com",
                'email_verified_at' => now(),
                'password' => "admin1234",
                'remember_token' => \Illuminate\Support\Str::random(10),
                'active' => "on"
            ],
            [
                'name' => "editor",
                'email' => "editor@adecoagro.com",
                'email_verified_at' => now(),
                'password' => "edit1234",
                'remember_token' => \Illuminate\Support\Str::random(10),
            ],
        ];
        foreach ($users as $user) {
            \App\User::query()->updateOrCreate(
                ["name" => $user["name"], "email" => $user["email"]],
                $user
            )->assignRole($user["name"]);
        }
    }
}
