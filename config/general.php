<?php

return [
    "default-password" => 1234,
    "language" => [
        "es" => "Español",
        "en" => "Inglés",
        "fr" => "Francés",
    ]
];
